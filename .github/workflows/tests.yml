name: Tests

on: [push, pull_request]

jobs:
  lint:
    runs-on: ubuntu-latest
    container:
      image: ghcr.io/thombashi/python-tox:3.8
    permissions: read-all
    concurrency:
      group: ${{ github.workflow }}-${{ github.job }}
      cancel-in-progress: true
    timeout-minutes: 20

    steps:
      - uses: actions/checkout@v2

      - name: Lint
        run: make check

  unit-test:
    runs-on: ${{ matrix.os }}
    strategy:
      fail-fast: false
      matrix:
        python-version: ["3.6", "3.7", "3.8", "3.9", "3.10", pypy3]
        os: [ubuntu-latest, macos-latest, windows-latest]
        exclude:
          - os: windows-latest
            python-version: 3.6
          - os: macos-latest
            python-version: pypy3
    timeout-minutes: 20

    steps:
      - uses: actions/checkout@v2

      - name: Setup Python ${{ matrix.python-version }}
        uses: actions/setup-python@v2
        with:
          python-version: ${{ matrix.python-version }}

      - name: Install pip
        run: python -m pip install --upgrade --disable-pip-version-check "pip>=21.1"

      - name: Get pip cache dir
        id: pip-cache
        run: echo "::set-output name=dir::$(pip cache dir)"

      - name: Cache pip
        uses: actions/cache@v2
        with:
          path: |
            ${{ steps.pip-cache.outputs.dir }}
            ./.tox
          key: ${{ matrix.os }}-${{ matrix.python-version }}-pip-${{ hashFiles('setup.py', '**/requirements.txt') }}
          restore-keys: |
            ${{ matrix.os }}-${{ matrix.python-version }}-pip-

      - name: Install dependencies
        run: make setup-ci

      - name: Run tests
        run: tox -e py
        env:
          PYTEST_DISCORD_WEBHOOK: ${{ secrets.PYTEST_DISCORD_WEBHOOK }}
